// from comment at https://gist.github.com/mudge/5830382
// based on https://nodejs.org/docs/latest-v5.x/api/events.html#events_event_newlistener
// explained at https://code.tutsplus.com/tutorials/using-nodes-event-module--net-35941

/*
 * This contains almost all of the specification found in the node.js docs
 * except for the ability to set a maximum number of listeners.
 */

const eventify = function (self) {
	self.events = {}

	self.on = function (event, listener) {
		if (typeof self.events[event] !== 'object') {
			self.events[event] = [];
		}

		self.emit('newListener', event, listener);

		self.events[event].push(listener);

		return self;
	}

	self.addListener = self.on;

	self.removeListener = function (event, listener) {
		let idx

		if (typeof self.events[event] === 'object') {
			idx = self.events[event].indexOf(listener)

			if (idx > -1) {
				self.events[event].splice(idx, 1)
				self.emit('removeListener', event, listener);
			}
		}

		return self;
	}

	self.emit = function (event) {
		var i, listeners, length, args = [].slice.call(arguments, 1);

		if (typeof self.events[event] === 'object') {
			listeners = self.events[event].slice()
			length = listeners.length

			for (i = 0; i < length; i++) {
				listeners[i].apply(self, args)
			}

			return true; // event had listeners
		}
		else { return false; }
	}

	self.once = function (event, listener) {
		self.on(event, function g () {
			self.removeListener(event, g)
			listener.apply(self, arguments)
		});
	}

	self.removeAllListeners = function (event) {
		if (event) {
			delete self.events[event];
		} else {
			Object
				.keys(self.events)
				.forEach((key) => delete self.events[key]);
		}

		return self;
	}

	self.listenerCount = function (event) {
		if (typeof self.events[event] !== 'object') {
			return 0;
		}

		return self.events[event].length;
	}

	self.listeners = function (event) {
		if (typeof self.events[event] !== 'object') {
			return [];
		}

		return self.events[event].slice(); // shallow copy
	}
}
