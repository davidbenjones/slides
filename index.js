// TODO: select lineWidth, strokeStyle
// TODO: add save button
// TODO: https://usefulangle.com/post/90/javascript-pdfjs-enable-text-layer
// https://tex.stackexchange.com/questions/161094/adding-custom-metadata-values-to-a-pdf-file
// https://ctan.org/pkg/hyperxmp?lang=en

// IDEA: allow for automatic camera to appear using custom page labels
// if metadata falls through

/*
 * New functions:
 *     lockControls
 *     drawOn
 *     drawOff
 */

/* New events (other than for above functions):
 *     PDFload
 *
 */

(function () {
	var reader = new FileReader();
	var fileInput = null;
	var pdfjsLib = window['pdfjs-dist/build/pdf'];
	var tools = {};
	var currentTool = "";
	var coordStack = [];
	var pages = [];
	var currentIndex = 0;
	var blank = false;
	var pageChangeEnabled = false;
	var viewportWidth = 0;
	var viewportHeight = 0;

	pdfjsLib.GlobalWorkerOptions.workerSrc =
		'https://cdn.jsdelivr.net/npm/pdfjs-dist@2.1.266/build/pdf.worker.min.js';

	function promptFile() {
		document.getElementById('modal-backdrop').setAttribute('class', 'file');
		var modal = document.getElementById('modal-backdrop');

		fileInput.addEventListener('change',
			function () { 
				modal.setAttribute('class', 'hidden-modal');
				reader.onload = loadPDF;
				reader.readAsArrayBuffer(fileInput.files[0]);
			});
	}

	function loadPDF () {
		pdfjsLib
			.getDocument({ data : reader.result })
			.promise
			.then(setupPDF, (reason) => console.error(reason));
	}

	function setupPDF (pdf) {
		window.slides.pdf = pdf;
		pages = new Array(pdf.numPages)
			.fill(null)
			.map((item, index) => {
				return {
					pdfPage : pdf.getPage(index + 1),
					drawStack : [],
					redoStack : [],
					label : null,
					linked : false,
				};
			});

		Promise
			.all(pages.map((item) => { return item.pdfPage; }))
			.then((items) => {
				items.forEach((item, index) => {
					pages[index].pdfPage = item;
				});

				goToPageIndex(0);
				window.slides.emit('drawOn');
			});

		pdf.getPageLabels().then(
			function (labels) {
				if (labels) {
					labels.forEach(function (label, index) {
						pages[index].label = label;
					});
					linkStacks();
				}
			});
	}

	function linkStacks () {
		pages.forEach((page, index) => {
			var thisPage = pages[index];
			var prevPage = pages[index - 1] || { label : null };

			if (thisPage.label === prevPage.label) {
				thisPage.drawStack = prevPage.drawStack;
				thisPage.redoStack = prevPage.redoStack;
				thisPage.linked = true;
				prevPage.linked = true;
			}
		});
	}

	function setCanvasDimensions (realWidth, realHeight, viewportWidth, viewportHeight) {
		slides.slideCanvas.height = viewportHeight;
		slides.slideCanvas.width = viewportWidth;

		slides.drawCanvas.height = viewportHeight;
		slides.drawCanvas.width = viewportWidth;

		slides.tempCanvas.height = viewportHeight;
		slides.tempCanvas.width = viewportWidth;

		var scale = Math.min(realWidth/viewportWidth,realHeight/viewportHeight);
		slides.slideCanvas.parentNode.style.width = Math.floor(viewportWidth * scale) + 'px';
		slides.slideCanvas.parentNode.style.height = Math.floor(viewportHeight * scale) + 'px';
	}

	function renderPDFPage (page) {
		var viewport = page.getViewport({ scale: slides.resolution });
		viewportWidth = viewport.width;
		viewportHeight = viewport.height;

		var destWidth = document.documentElement.clientWidth;
		var destHeight = document.documentElement.clientHeight;

		setCanvasDimensions(destWidth, destHeight, viewportWidth, viewportHeight);

		// Render PDF page into canvas context
		var renderContext = {
		  canvasContext: slides.slideCanvas.getContext('2d'),
		  viewport: viewport
		};

		return page.render(renderContext).promise;
	}

	function clearCanvas (canvas) {
		canvas.height = canvas.height;
	}

	function clear () {
		var page = pages[currentIndex];

		clearCanvas(slides.drawCanvas);

		var draw = function (canvas) { clearCanvas(canvas); };
		page.drawStack.push(draw);

		page.redoStack = [];

		slides.emit('drawEnd', draw);
	}

	function toggleFullscreen () {
		var element = document.body;
		element.requestFullscreen = element.requestFullscreen ||
			element.mozRequestFullScreen ||
			element.webkitRequestFullscreen ||
			element.msRequestFullscreen;

		if (document.fullscreenElement ||
			document.webkitFullscreenElement ||
			document.mozFullScreenElement ||
			document.msFullscreenElement) {

			if (document.exitFullscreen) { document.exitFullscreen(); }
			else if (document.webkitExitFullscreen) { document.webkitExitFullscreen(); }
			else if (document.mozCancelFullScreen) { document.mozCancelFullScreen(); }
			else if (document.msExitFullscreen) { document.msExitFullscreen(); }

		}
		else {
			document.body.requestFullscreen(document.body.ALLOW_KEYBOARD_INPUT);
		}
	}

	function fullscreenChange () {
		if (document.fullscreenElement ||
			document.webkitFullscreenElement ||
			document.mozFullScreenElement ||
			document.msFullscreenElement) {
			// Entering fullscreen
			slides.emit('enterFullscreen');
			setTimeout(function () { goToPageIndex(slides.currentIndex); }, 100);
		}
		else {
			// Leaving fullscreen
			slides.emit('leaveFullscreen');
			setTimeout(function () { goToPageIndex(slides.currentIndex); }, 100);
		}
	}

	document.addEventListener('keydown', handleBPress);

	function enterFullscreen ()  {}
	function leaveFullscreen () {}

	/*
	 * TODO:
	 * This needs to be fixed. There should be an easy
	 * way to enable/disable changing the slide, with
	 * an event handler to go with it (for updates to
	 * the toolbar).
	 *
	 * Perhaps disableNext/enableNext etc. ?
	 */
	function enablePageChange () {
		document.addEventListener('keydown', handleArrowPress);
		pageChangeEnabled = true;
		slides.emit('enablePageChange');
	}
	function disablePageChange () {
		document.removeEventListener('keydown', handleArrowPress);
		pageChangeEnabled = false;
		slides.emit('disablePageChange');
	}

	function handleArrowPress (event) {
		if (event.defaultPrevented) {
			return;
		}

		var key = event.key || event.keyCode;

		switch (key) {
			case "ArrowRight":
			case "Right":
			case "ArrowDown":
			case "Down":
			case "PageDown":
			case "Enter":
			case " ":
				nextSlide();
				event.stopPropagation();
				event.preventDefault();
				break;
			case "ArrowLeft":
			case "Left":
			case "ArrowUp":
			case "Up":
			case "PageUp":
			case "Backspace":
				prevSlide();
				event.stopPropagation();
				event.preventDefault();
				break;
		}
	}

	function handleBPress (event) {
		if (event.defaultPrevented) {
			return;
		}

		var key = event.key || event.keyCode;

		switch (key) {
			case "b":
				toggleBlank();
				event.stopPropagation();
				event.preventDefault();
				break;
		}
	}

	/*
	 * Event: 'pagechange'
	 * - new page object
	 * - new page index
	 */
	function goToPageIndex (index) {
		if (!pageChangeEnabled) { return; }
		var eventName = slides.currentIndex - index === 0 ? 'pageReload' : 'pageChange';

		if (0 <= index && index < pages.length) {
			slides.currentIndex = index;
			var page = pages[index].pdfPage;
			var event = [eventName, pages[index], index];

			if (page) {
				renderPDFPage(page)
					.then(() => {
						redraw();

						slides.emit(...event);
					});
			}
			else {
				var destWidth = document.documentElement.clientWidth;
				var destHeight = document.documentElement.clientHeight;
				setCanvasDimensions(destWidth, destHeight, viewportWidth, viewportHeight);

				var canvas = slides.slideCanvas;
				var context = canvas.getContext('2d');
				context.fillStyle = 'white';
				context.fillRect(0, 0, canvas.width, canvas.height);

				clearCanvas(slides.drawCanvas);
				redraw();
				slides.emit(...event);
			}
		}
	}

	function nextSlide () { goToPageIndex(slides.currentIndex + 1); }
	function prevSlide () { goToPageIndex(slides.currentIndex - 1); }

	function toggleBlank() {
		if (!slides.pdf) { return; }// ignore if no file selected

		if (blank === false) {
			disablePageChange();
			document.getElementById('blank-screen').classList.remove('hidden');
		}
		else {
			// reload page on reveal just in case
			goToPageIndex(slides.currentIndex);
			enablePageChange();
			document.getElementById('blank-screen').classList.add('hidden');
		}
		blank = !blank;
	}

	/* Drawing */

	function  getMousePos (canvas, event) {
		var rect = canvas.getBoundingClientRect(), // abs. size of element
			scaleX = canvas.width / rect.width,    // relationship bitmap vs. element for X
			scaleY = canvas.height / rect.height;  // relationship bitmap vs. element for Y

		return {
			x: (event.clientX - rect.left) * scaleX,   // scale mouse coordinates after they have
			y: (event.clientY - rect.top) * scaleY     // been adjusted to be relative to element
		}
	}

	function addMousePos (coords) { coordStack.push(coords); }

	function mouseDown () {
		if (event.button != 0) { return; }

		slides.emit('drawStart');

		var pos = getMousePos(slides.drawCanvas, event);
		addMousePos(pos);

		var options = { inProgress : true };
		tools[currentTool].draw(slides.tempCanvas, coordStack, options);

		slides.tempCanvas.addEventListener('mousemove', mouseMove);
		slides.tempCanvas.addEventListener('mouseup', mouseUp);
		slides.tempCanvas.addEventListener('mouseleave', mouseLeave);
	}

	function mouseMove (event) {
		var pos = getMousePos(slides.drawCanvas, event);
		addMousePos(pos);

		clearCanvas(slides.tempCanvas);

		var options = { inProgress : true };
		tools[currentTool].draw(slides.tempCanvas, coordStack, options);
	}

	function saveDraw(draw, coordStack, options) {
		return function (stack, opts, canvas) {
			draw(canvas, stack, opts);
		}.bind(null, coordStack, options);
	}
	
	function mouseUp (event) {
		slides.tempCanvas.removeEventListener('mousemove', mouseMove);
		slides.tempCanvas.removeEventListener('mouseup', mouseUp);
		slides.tempCanvas.removeEventListener('mouseleave', mouseLeave);

		clearCanvas(slides.tempCanvas);

		var options = {
			lineWidth : slides.lineWidth,
			lineJoin : slides.lineJoin,
			lineCap : slides.lineCap,
			strokeStyle : slides.strokeStyle,
			inProgress : false,
		}


		var draw = saveDraw(tools[currentTool].draw, coordStack, options);
		draw(slides.drawCanvas);

		pages[currentIndex].drawStack.push(draw);
		pages[currentIndex].redoStack = [];
		coordStack = [];

		slides.emit('drawEnd',
			draw,
			coordStack,
			options);
	}

	function mouseLeave (event) { mouseUp(event); }

	function redraw () {
		pages[currentIndex]
			.drawStack
			.forEach((draw) => { draw(slides.drawCanvas); });
		slides.emit('redraw');
	}

	function addPage (index) {
		var index = index || slides.currentIndex + 1; // default after current page
		var page = {
			pdfPage : null,
			drawStack : [],
			redoStack : [],
			label : null,
			linked : false,
		}

		slides.pages.splice(index, 0, page);
		slides.emit(
			'addpage',
			page,
			index);
		goToPageIndex(index); // emits 'pagechange' event
	}

	function removePage (index) {
		var index = index || slides.currentIndex; // default current page
		goToPageIndex(slides.currentIndex - 1); // emits 'pagechange' event

		var page = slides.pages.splice(index, 1);
		slides.emit(
			'removepage',
			page,
			index);
		
	}

	function undo () {
		var page = pages[currentIndex];
		var draw = page.drawStack.pop();
		page.redoStack.push(draw);
		clearCanvas(slides.drawCanvas);
		redraw();

		slides.emit('undo',
			draw,
			page.drawStack,
			page.redoStack);
	}

	function redo () {
		var page = pages[currentIndex];
		var draw = page.redoStack.pop();
		page.drawStack.push(draw);
		draw();

		slides.emit('redo',
			draw,
			page.drawStack,
			page.redoStack);
	}

	function getTool () { return currentTool; }
	function setTool (name) {
		if (currentTool) { tools[currentTool].offSelect(); }
		if (tools[name]) { tools[name].onSelect(); }
		currentTool = name;
	}

	function addTool (tool) { tools[tool.name] = tool; }
	function removeTool (name) { delete tools[name]; }

	function init () {
		slides.slideCanvas = document.getElementById('slide-canvas');
		slides.drawCanvas = document.getElementById('draw-canvas');
		slides.tempCanvas = document.getElementById('temp-canvas');

		slides.tempCanvas.addEventListener('mousedown', mouseDown);

		fileInput = document.getElementById('file-input');

		document.addEventListener('fullscreenchange', fullscreenChange);
		document.addEventListener('webkitfullscreenchange', fullscreenChange);
		document.addEventListener('mozfullscreenchange', fullscreenChange);

		slides.on('enterFullscreen', enterFullscreen);
		slides.on('leaveFullscreen', leaveFullscreen);

		var initEvent = new CustomEvent('slides-ready');
		window.dispatchEvent(initEvent);

		slides.tool = 'pen';
		enablePageChange();

		promptFile();
	}

	/* public interface */
	window.slides = {
		init : init,
		resolution : 5,
		slideCanvas : null,
		drawCanvas: null,
		tempCanvas : null,
		pdf : null,
		get pages () { return pages; },
		get currentIndex () { return currentIndex; },
		set currentIndex (num) { currentIndex = num; },

		strokeStyle : "blue",
		lineJoin : "round",
		lineCap : "round",
		lineWidth : 5,

		nextSlide : nextSlide,
		prevSlide : prevSlide,

		undo : undo,
		redo : redo,

		clear : clear,

		toggleFullscreen : toggleFullscreen,

		set tool (name) { setTool(name); },
		get tool () { return getTool(); },
		get tools () { return tools; },
		addTool : addTool,
		removeTool : removeTool,

		enablePageChange : enablePageChange,
		disablePageChange : disablePageChange,
		get pageChangeEnabled () { return pageChangeEnabled; },

		toggleBlank : toggleBlank,
		addPage : addPage,
		removePage : removePage,
	};

	eventify(window.slides);

	window.addEventListener('load', slides.init);
})();
