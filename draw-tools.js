
/*
 * tool object:
 * {
 *    name,
 *    draw,
 *    onSelect,
 *    offSelect,
 * }
 */

(function () {
	var scale = 12;
	var button = null;
	var cursor = document.createElement('canvas');

	function erasePen (canvas, coords, options) {
		var context = canvas.getContext('2d');

		var options = options || {};
		context.strokeStyle = options.strokeStyle || slides.strokeStyle;
		context.lineJoin    = options.lineJoin || slides.lineJoin;
		context.lineCap     = options.lineCap || slides.lineCap;
		context.lineWidth   = options.lineWidth || slides.lineWidth;

		if (options.inProgress) {
			context.strokeStyle = 'rgba(0,0,0,0.5)';
		} else {
			context.globalCompositeOperation = 'destination-out';
		}

		context.beginPath();
		context.moveTo(coords[0].x,coords[0].y);
		for (let i = 0; i < coords.length; i++) {
			context.lineTo(coords[i].x,coords[i].y);
		}
		context.stroke();
	}

	function onSelect () {
		button.classList.add('selected');
		slides.lineWidth = scale * slides.lineWidth;

		var lineWidth = 3;
		var width = slides.lineWidth/2;

		cursor.width = width + 2*lineWidth;
		cursor.height = width + 2*lineWidth;

		var center = Math.ceil(width/2) + lineWidth/2;
		var ctx = cursor.getContext('2d');
		ctx.lineWidth = 3;
		ctx.strokeStyle = 'black';

		ctx.beginPath();
		ctx.arc(
			center,
			center,
			center - lineWidth/2,
			0, 2*Math.PI);
		ctx.stroke();

		var d = cursor.toDataURL();
		slides.tempCanvas.style.cursor =
			'url(' + d + ')' +
			center + ' ' +
			center + ', auto';
	}

	function offSelect () {
		button.classList.remove('selected');
		slides.lineWidth = Math.floor(slides.lineWidth / 12);
	}

	var eraser = {
		name : 'eraser',
		draw : erasePen,
		onSelect : onSelect,
		offSelect : offSelect,
	}

	function initEraser () {
		button = document.getElementById('toolbar-erase');
		button.addEventListener('click', function () { slides.tool = "eraser"; });
		slides.addTool(eraser);
	}

	window.addEventListener('slides-ready', initEraser);
})();

(function () {
	var cursor = document.createElement('canvas');
	var button = null;

	function drawPen (canvas, coords, options) {
		var context = canvas.getContext('2d');

		var options = options || {};
		context.strokeStyle = options.strokeStyle || slides.strokeStyle;
		context.lineJoin    = options.lineJoin || slides.lineJoin;
		context.lineCap     = options.lineCap || slides.lineCap;
		context.lineWidth   = options.lineWidth || slides.lineWidth;

		context.globalCompositeOperation = 'source-over';

		context.beginPath();
		if (coords.length === 1) {
			context.fillStyle = context.strokeStyle;
			context.arc(
				coords[0].x,
				coords[0].y,
				options.lineWidth/2,
				0, 2 * Math.PI);
			context.fill();
			return;
		}

		context.moveTo(coords[0].x,coords[0].y);
		for (let i = 0; i < coords.length; i++) {
			context.lineTo(coords[i].x,coords[i].y);
		}
		context.stroke();
	}

	function onSelect () {
		button.setAttribute('class', 'selected');

		cursor.width = slides.lineWidth;
		cursor.height = slides.lineWidth;

		var center = Math.floor(cursor.width/2);
		var ctx = cursor.getContext('2d');
		ctx.fillStyle = slides.strokeStyle;
		ctx.strokeStyle = slides.strokeStyle;
		ctx.lineWidth = slides.lineWidth;
		ctx.beginPath();
		ctx.arc(
			center,
			center,
			center,
			0, 2*Math.PI);
		ctx.fill();

		var d = cursor.toDataURL();
		slides.tempCanvas.style.cursor =
			'url(' + d + ')' +
			center + ' ' +
			center + ', auto';
	}

	function offSelect () {
		button.removeAttribute('class');
	}

	var pen = {
		name : 'pen',
		draw : drawPen,
		onSelect : onSelect,
		offSelect : offSelect,
	}

	function init () {
		button = document.getElementById('toolbar-pen');
		button.addEventListener('click', function () { slides.tool = "pen"; });
		slides.addTool(pen);
	}

	window.addEventListener('slides-ready', init);
})();
