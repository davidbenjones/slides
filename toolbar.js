/*
 * TODO: remove arrow event handlers on document camera
 */

(function () {
	var toolbar = {};
	var toolbarPinned = false;

	function togglePinToolbar () {
		if (toolbarPinned) {
			toolbar.pin.classList.remove('selected');
			toolbar.container.addEventListener('mouseover', showToolbar);
			toolbar.container.addEventListener('mouseout', hideToolbar);
		} else {
			toolbar.pin.classList.add('selected');	
			toolbar.container.removeEventListener('mouseover', showToolbar);
			toolbar.container.removeEventListener('mouseout', hideToolbar);
		}
		toolbarPinned = !toolbarPinned;
	}

	function drawStart () { toolbar.container.classList.add('hidden'); }
	function drawEnd   (draw) {
		var i = slides.currentIndex;
		var page = slides.pages[i];

		toolbar.container.classList.remove('hidden');
		undoRedo(draw, page.drawStack, page.redoStack);
	}

	function undoRedo (draw, drawStack, redoStack) {
		redoStack.length === 0 ? disableRedo() : enableRedo();
		drawStack.length === 0 ? disableUndo() : enableUndo();
	}

	function pageChange (page, index) {
		undoRedo(null, page.drawStack, page.redoStack);

		index === 0 ? disablePrev() : enablePrev();
		index === slides.pages.length - 1 ? disableNext() : enableNext();
	}

	function disableArrows () { disableNext(); disablePrev(); }
	function enableArrows () { enableNext(); enablePrev(); }

	function showToolbar () { toolbar.self.classList.remove('hidden'); }
	function hideToolbar () { toolbar.self.classList.add('hidden'); }

	function disableNext () { toolbar.next.setAttribute('disabled',''); }
	function  enableNext () { toolbar.next.removeAttribute('disabled'); }
	function disablePrev () { toolbar.prev.setAttribute('disabled',''); }
	function  enablePrev () { toolbar.prev.removeAttribute('disabled'); }

	function disableRedo () { toolbar.redo.setAttribute('disabled',''); }
	function  enableRedo () { toolbar.redo.removeAttribute('disabled'); }
	function disableUndo () { toolbar.undo.setAttribute('disabled',''); }
	function  enableUndo () { toolbar.undo.removeAttribute('disabled'); }

	function disableNew () { toolbar.new.setAttribute('disabled',''); }
	function  enableNew () { toolbar.new.removeAttribute('disabled'); }

	function leaveFullscreen () { 
		toolbar.full.setAttribute('title', 'Enter Fullscreen');
		toolbar.full
			.getElementsByTagName('img')[0]
			.setAttribute('src', 'images/full-screen-alt-1.png');
	}
	function enterFullscreen () {
		toolbar.full.setAttribute('title', 'Exit Fullscreen');
		toolbar.full
			.getElementsByTagName('img')[0]
			.setAttribute('src', 'images/full-screen-exit-alt-1.png');
	}

	function init () {
		toolbar.full   = document.getElementById('toolbar-full');
		toolbar.next   = document.getElementById('toolbar-next');
		toolbar.prev   = document.getElementById('toolbar-prev');
		toolbar.undo   = document.getElementById('toolbar-undo');
		toolbar.redo   = document.getElementById('toolbar-redo');
		toolbar.clear  = document.getElementById('toolbar-clear');
		toolbar.pin    = document.getElementById('toolbar-pin');
		toolbar.new    = document.getElementById('toolbar-new');
		toolbar.self   = document.getElementById('toolbar');
		toolbar.container = document.getElementById('toolbar-container');

		toolbar.pin   .addEventListener('click', togglePinToolbar);
		toolbar.full  .addEventListener('click', slides.toggleFullscreen);
		toolbar.clear .addEventListener('click', slides.clear);
		toolbar.next  .addEventListener('click', slides.nextSlide);
		toolbar.prev  .addEventListener('click', slides.prevSlide);
		toolbar.undo  .addEventListener('click', slides.undo);
		toolbar.redo  .addEventListener('click', slides.redo);
		toolbar.new   .addEventListener('click', function () { slides.addPage(); });
		// wrapped function because events send unwanted arguments

		toolbar.container.addEventListener('mouseover', showToolbar);
		toolbar.container.addEventListener('mouseout', hideToolbar);

		slides.addListener('enterFullscreen', enterFullscreen);
		slides.addListener('leaveFullscreen', leaveFullscreen);

		slides.addListener('drawStart', drawStart);
		slides.addListener('drawEnd',   drawEnd);

		slides.addListener('undo', undoRedo);
		slides.addListener('redo', undoRedo);

		slides.addListener('enablePageChange',  enableArrows);
		slides.addListener('disablePageChange', disableArrows);

		slides.addListener('pageChange', pageChange);
		slides.addListener('pageReload', pageChange);

		slides.toolbar = toolbar;

		var initEvent = new CustomEvent('slides-toolbar-ready');
		window.dispatchEvent(initEvent);
	}

	window.addEventListener('slides-ready', init);
})();
