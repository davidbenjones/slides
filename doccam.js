/*
 * TODO: Add camera button that takes a snapshot of
 * the current doccam/drawings and saves the photo
 * as the previous slide.
 *
 * https://stackoverflow.com/questions/4773966/drawing-an-image-from-a-data-url-to-a-canvas
 * https://stackoverflow.com/questions/13760805/how-to-take-a-snapshot-of-html5-javascript-based-video-player
 *
 * This would involve getting the image of the screen, saving an image draw function,
 * copying the draw stack for the slide (reset draw stack afterward?)
 */

(function () {

	var toolbar = null;
	var viewport = null;
	var docCamStream = null;

	function toggleDocCam () {
		if (docCamStream) {
			viewport.classList.add('hidden');
			docCamStream.stop();
			docCamStream = null;
			viewport.srcObject = null;

			toggleShowPlayPause();
			slides.enablePageChange();

			slides.removePage();
			toolbar.doccam.classList.remove('selected');
		} else {
			navigator
				.mediaDevices
				.getUserMedia({ video : { width: 1280, height: 720 } })
				.then(function (stream) {
					docCamStream = stream.getTracks()[0];
					viewport.classList.remove('hidden');
					viewport.srcObject = stream;
					playPause();
					toggleShowPlayPause();

					slides.addPage();

					slides.disablePageChange();
					toolbar.doccam.classList.add('selected');
				});
		}
	}

	function toggleShowPlayPause () {
		if (toolbar.pause.classList.contains('hidden')) {
			toolbar.new.classList.add('hidden');
			toolbar.pause.classList.remove('hidden');
		} else {
			toolbar.new.classList.remove('hidden');
			toolbar.pause.classList.add('hidden');
		}
	}

	function playPause () {
		if (viewport.paused) {
			viewport.play();
			toolbar.pause
				.getElementsByTagName('img')[0]
				.setAttribute('src', 'images/pause.png');
			toolbar.pause.setAttribute('title', 'Pause Document Camera');
		} else {
			viewport.pause();
			toolbar.pause
				.getElementsByTagName('img')[0]
				.setAttribute('src', 'images/play.png');
			toolbar.pause.setAttribute('title', 'Play Document Camera');
		}
	}

	function init () {
		toolbar = slides.toolbar;
		viewport = document.getElementById('doc-cam');

		toolbar.doccam = document.getElementById('toolbar-doccam');
		toolbar.pause  = document.getElementById('toolbar-pause');

		toolbar.doccam.addEventListener('click', toggleDocCam);
		toolbar.pause .addEventListener('click', playPause);

		toolbar.pause.classList.add('hidden');
	}

	window.addEventListener('slides-toolbar-ready', init);
})();
